<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Modelo1;
use app\models\Modelo2;
use app\models\Modelo3;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionEjercicio1()
    {
        $model = new Modelo1();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                for($x=1;$x<=$model->numero;$x++)
                {
                    $numeros[]=$x;
                }
                return $this->render('resultadoEjercicio1y2', ['numeros' => $numeros]);
            }
        }

        return $this->render('ejercicio1y2', ['model' => $model]);
    }
    
    public function actionEjercicio2()
    {
        $model = new Modelo2();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                for($x=$model->numero;$x>=1;$x--)
                {
                    $numeros[]=$x;
                }
                return $this->render('resultadoEjercicio1y2', ['numeros' => $numeros]);
            }
        }

        return $this->render('ejercicio1y2', ['model' => $model]);
    }
    
    public function actionEjercicio3()
    {
        $model = new Modelo3();

        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $resultado=$model->numero1+$model->numero2+$model->numero3;
                return $this->render('resultadoEjercicio3y4', ['resultado' => $resultado,'operacion' => 'suma']);
            }
        }

        return $this->render('ejercicio3y4', ['model' => $model,'operacion' => 'SUMAR']);
    }
    
    public function actionEjercicio4()
    {
        $model = new Modelo3();

        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $resultado=$model->numero1*$model->numero2*$model->numero3;
                return $this->render('resultadoEjercicio3y4', ['resultado' => $resultado,'operacion' => 'multiplicacion']);
            }
        }

        return $this->render('ejercicio3y4', ['model' => $model,'operacion' => 'MULTIPLICAR']);
    }
}
