<?php

namespace app\models;

use yii\base\Model;

class Modelo1 extends Model
{
    public $numero;

    public function rules()
    {
        return [
            [['numero'], 'required'],
            [['numero'],'number','min'=>1,'max'=>100],
        ];
    }
    
    public function attributeLabels()
    {
        return ['numero' => 'NUMERO'];
    }
}