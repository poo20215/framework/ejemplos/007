<?php

namespace app\models;

use yii\base\Model;

class Modelo2 extends Model
{
    public $numero;

    public function rules()
    {
        return [
            [['numero'], 'required'],
            [['numero'],'number','min'=>10,'max'=>100],
        ];
    }
    
    public function attributeLabels()
    {
        return ['numero' => 'NUMERO'];
    }
}