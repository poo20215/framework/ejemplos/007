<?php

namespace app\models;

use yii\base\Model;

class Modelo3 extends Model
{
    public $numero1;
    public $numero2;
    public $numero3;

    public function rules()
    {
        return [
            [['numero1','numero2','numero3'], 'required'],
            [['numero1','numero2','numero3'],'number'],
            ['numero3','comprobacion']
        ];
    }
    
    public function comprobacion($attribute,$params)
    {
        if($this->$attribute<=$this->numero2)
        {
            $this->addError($attribute,"El NUMERO3 tiene que se mayor que el NUMERO2");
        }
    }
    
    public function attributeLabels()
    {
        return ['numero1' => 'NUMERO1',
                'numero2' => 'NUMERO2',
                'numero3' => 'NUMERO3'];
    }
}